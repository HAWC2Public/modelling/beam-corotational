from setuptools import setup, find_packages

setup(
    name='beam_corot',
    description='',
    author='DTU Wind Energy',
    include_package_data=True,    
    install_requires=["numpy", "scipy"],
    extras_require={
    },
    zip_safe=False,
    version='0.1',
    packages=find_packages(),
    package_data= {"": [
        ]}
)
